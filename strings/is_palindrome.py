def isPalindrome(A):
    import string
    ltrs = string.ascii_letters
    new_str = filter(lambda x: x in ltrs or x in string.digits, A)
    print('new_str is {}'.format(new_str))
    for i in range(len(new_str)/2):
        if new_str[i].lower() != new_str[-1-i].lower():
            return 0
    return 1

if __name__ == '__main__':
    print isPalindrome('1a2')
