"""
Given a non-empty string s, you may delete at most one character.
Judge whether you can make it a palindrome.
"""

def validPalindrome(s):
    i = 0
    j = len(s) - 1
    while i <= j:
        if s[i] != s[j]:
            a = [x for x in s]
            b = [x for x in s]
            a.pop(i)
            b.pop(j)
            return a == a[::-1] or b == b[::-1]
        else:
            i +=1
            j -= 1
    return True

if __name__ == '__main__':
    assert validPalindrome('aba') == True
    assert validPalindrome('abca') == True
    assert validPalindrome('abc') == False
    assert validPalindrome('eedede') == True
    assert validPalindrome('abab') == True
    assert validPalindrome('abaad') == False
    print('tests run successfully')
