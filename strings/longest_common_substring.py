"""
Given two strings A and B, find the length of the longest common substring

References:
- http://www.geeksforgeeks.org/longest-common-substring/

        A   B   C   D
    0   0   0   0   0
C   0   0   0   1   0
B   0   0   1   0   0
C   0   0   0   2   0
E   0   0   0   0   0
"""

def common_substring(A, B):
    m = 0
    f = [[0 for i in range(len(B))] for j in range(len(A))]
    if len(A) == 0:
        return 0
    if len(B) == 0:
        return 0
    for i in range(1, len(A)):
        for j in range(1, len(B)):
            if A[i] != B[j]:
                f[i][j] = 0
            else:
                f[i][j] = f[i-1][j-1] + 1
            if m < f[i][j]:
                m = f[i][j]
    return m

if __name__ == '__main__':
    A = "ABCD"
    B = "CBCE"
    assert common_substring(A, B) == 2
    A = "OldSite:GeeksforGeeks.org";
    B = "NewSite:GeeksQuiz.com";
    assert common_substring(A, B) == 10
