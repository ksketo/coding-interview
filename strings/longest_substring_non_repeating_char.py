"""
Given a string, find the length of the longest substring without repeating characters.

Examples:
Given "abcabcbb", the answer is "abc", which the length is 3.
Given "bbbbb", the answer is "b", with the length of 1.
Given "pwwkew", the answer is "wke", with the length of 3. Note that the answer
must be a substring, "pwke" is a subsequence and not a substring.
"""

def lengthOfLongestSubstring(s):
    """
    :type s: str
    :rtype: int
    """
    start = 0
    r = set([])
    m_l = 0
    for char in s:
        while char in r:
            r.remove(s[start])
            start += 1
        r.add(char)
        if len(r) > m_l:
            m_l = len(r)
    return m_l


if __name__ == '__main__':
    assert lengthOfLongestSubstring("abcabcbb") == 3
    assert lengthOfLongestSubstring("aab") == 2
    assert lengthOfLongestSubstring("bbbb") == 1
    assert lengthOfLongestSubstring("dvdf") == 3
    print('tests run successfully')
