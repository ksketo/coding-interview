"""
Check for validity
https://stackoverflow.com/questions/267399/how-do-you-match-only-valid-roman-numerals-with-a-regular-expression/267405#267405
"""

import re

value_dict = {
    'I': 1,
    'V': 5,
    'X': 10,
    'L': 50,
    'C': 100,
    'D': 500,
    'M': 1000
}

def is_valid(string):
  if len(string) == 0:
    return False
  valid_roman_regex = '^M*(C[MD]|D?C{0,3})(X[CL]|L?X{0,3})(I[XV]|V?I{0,3})$'
  if not (set(string).issubset(set(value_dict.keys())) and re.search(valid_roman_regex, string)):
    return False
  return True

def romanToInt(A):

    if not is_valid(A):
        return None

    value = 0
    i = 0
    while i < len(A):
        if i+1 < len(A):
            if value_dict[A[i]] >= value_dict[A[i+1]]:
                value += value_dict[A[i]]
                i += 1
            else:
                value += value_dict[A[i+1]] - value_dict[A[i]]
                i += 2
        else:
            value += value_dict[A[i]]
            i += 1
    return value

if __name__ == '__main__':
    # valid
    assert romanToInt("XIV") == 14
    assert romanToInt("XIII") == 13
    assert romanToInt("XX") == 20
    assert romanToInt("CCVII") == 207
    assert romanToInt("MLXVI") == 1066
    assert romanToInt("XL") == 40
    assert romanToInt("XC") == 90
    assert romanToInt("MCMIV") == 1904
    # invalid
    print('tests passed successfully')
