def solve(A):
    for j in range(len(A)):
        max_length = len(A) - j
        if A[:len(A)-j] == A[:len(A)-j][::-1]:
            break
    return len(A) - max_length

if __name__ == '__main__':
    print('Result: {}'.format(solve("babb")))
    print('Result: {}'.format(solve("aaaaa")))
