def longestPalindrome(A):
    """
    Brute Force method
    Time Complexity: O(n^3)
    """
    for i in range(len(A)/2):
        if A[i] != A[-1-i]:
            break
    else:
        return A

    return max(longestPalindrome(A[:-1]), longestPalindrome(A[1:]), key=len)

# def longestPalindromeCS(A):
#     """
#     Method: Reverse String and find common substring with same indices
#     Time Complexity: O(n^2)
#     Space Complexity: O(n)
#     """

# def lps(A):
#     """
#     Method: DP - needs to be improved
#     """
#     n = len(A)
#
#     # Create a table to store results of subproblems
#     L = [[0 for x in range(n)] for x in range(n)]
#
#     # Strings of length 1 are palindrome of length 1
#     for i in range(n):
#         L[i][i] = 1
#
#     # Build the table. Note that the lower diagonal values of table are
#     # useless and not filled in the process. The values are filled in a
#     # manner similar to Matrix Chain Multiplication DP solution (See
#     # http://www.geeksforgeeks.org/dynamic-programming-set-8-matrix-chain-multiplication/
#     # cl is length of substring
#     for cl in range(2, n+1):
#         for i in range(n-cl+1):
#             j = i+cl-1
#             if A[i] == A[j] and cl == 2:
#                 L[i][j] = 2
#             elif A[i] == A[j]:
#                 L[i][j] = L[i+1][j-1] + 2
#             else:
#                 L[i][j] = max(L[i][j-1], L[i+1][j]);
#
#     return L[0][n-1]

def longestPalSubstr(string):
    """
    Time Complexity: O(n^2)
    Space Complexity: O(1)
    link: http://www.geeksforgeeks.org/longest-palindromic-substring-set-2/
    """
    maxLength = 1

    start = 0
    length = len(string)

    low = 0
    high = 0

    # One by one consider every character as center point of
    # even and length palindromes
    for i in xrange(1, length):
        # Find the longest even length palindrome with center
        # points as i-1 and i.
        low = i - 1
        high = i
        while low >= 0 and high < length and string[low] == string[high]:
            if high - low + 1 > maxLength:
                start = low
                maxLength = high - low + 1
            low -= 1
            high += 1

        # Find the longest odd length palindrome with center
        # point as i
        low = i - 1
        high = i + 1
        while low >= 0 and high < length and string[low] == string[high]:
            if high - low + 1 > maxLength:
                start = low
                maxLength = high - low + 1
            low -= 1
            high += 1

    return string[start:start + maxLength]

if __name__ == '__main__':
    print(longestPalSubstr("abbcccbbbcaaccbababcbcabca"))
