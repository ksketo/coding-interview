"""
Reverse a linked list
"""

def reverse_list(head):
    prev = None
    current = head
    while current is not None:
        next_tmp = current.next
        current.next = prev
        prev = current
        current = next_tmp
    return prev

def reverse_list_rec(head):
    """
    :type head: ListNode
    :rtype: ListNode
    """
    curr = head
    prev = None
    return helper(curr, prev)

def helper(curr, prev):
    if curr is None:
        return prev
    else:
        tmp = curr.next
        curr.next = prev
        prev = curr
        curr = tmp
        return helper(curr, prev)
