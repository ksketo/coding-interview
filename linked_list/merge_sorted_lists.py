"""
Merge two sorted linked lists and return it as a new list. The new list should
be made by splicing together the nodes of the first two lists.
"""

def merge_lists(l1, l2):
    head = ListNode(0)
    current = head
    while l1 and l2:
        new_node = ListNode(l1.val)
        if l1.val > l2.val:
            new_node.val = l2.val
            l2 = l2.next
        else:
            l1 = l1.next
        current.next = new_node
        current = current.next
    if l1:
        current.next = l1
    elif l2:
        current.next = l2
    return head.next
