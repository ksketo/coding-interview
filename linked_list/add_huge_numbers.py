class ListNode(object):
  def __init__(self, x):
    self.value = x
    self.next = None

def addTwoHugeNumbers(a, b):
    # Make lists double linked
    cp_a = a
    N_a = 0
    while cp_a:
        cp_a = cp_a.next
        N_a += 1
    cp_b = b
    N_b = 0
    while cp_b:
        cp_b = cp_b.next
        N_b += 1

    # Start from end of list and add node by node
    carry = 0
    root = n = ListNode(0)
    i = 0
    while N_a > 0 or N_b > 0 or carry:
        print('iteration: {}'.format(i))
        i += 1
        print('N_a: {}'.format(N_a))
        print('N_b: {}'.format(N_b))
        print('carry: {}'.format(carry))
        if N_a > 0:
            print('a.value = {}'.format(a.value))
        if N_b > 0:
            print('b.value = {}'.format(b.value))
        v1 = v2 = 0
        if N_a > N_b:
            v1 = a.value
            a = a.next
            N_a -= 1
        elif N_b > N_a:
            v2 = b.value
            b = b.next
            N_b -= 1
        elif (N_a == N_b):
            v1 = a.value
            v2 = b.value
            a = a.next
            b = b.next
            N_a -= 1
            N_b -= 1

        carry, value = divmod(v1+v2+carry, 10000)
        n.next = ListNode(value)
        n.next.prev = n
        n = n.next
        if carry:
            n.prev.value += carry 
            carry = 0
    return root.next

if __name__ == '__main__':
    a_arr = [9876, 5432, 1999]
    b_arr = [1, 8001]
    a = ListNode(a_arr[0])
    cp_a = a
    for elem in a_arr[1:]:
        cp_a.next = ListNode(elem)
        cp_a = cp_a.next
    b = ListNode(b_arr[0])
    cp_b = b
    for elem in b_arr[1:]:
        cp_b.next = ListNode(elem)
        cp_b = cp_b.next
    addTwoHugeNumbers(a, b)
