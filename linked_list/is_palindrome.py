"""
Check if linked list is palindrome
"""

def isPalindrome(head):
    """
    :type head: ListNode
    :rtype: bool

    Note: current solution modifies linked list
    """
    if not head or not head.next:
        return True

    # Find mid of list
    slow, fast = head, head
    while fast and fast.next:
        slow = slow.next
        fast = fast.next.next

    # Reverse second half
    current = slow
    if fast:
        current = slow.next
    prev = None
    while current is not None:
        tmp = current.next
        current.next = prev
        prev = current
        current = tmp

    # Compare first and second half
    a = head
    b = prev
    while a and b:
        if a.val != b.val:
            return False
        a = a.next
        b = b.next

    return True
