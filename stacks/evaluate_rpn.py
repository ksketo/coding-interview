"""
Evaluate reverse polished notation

Examples:
["2", "1", "+", "3", "*"] -> ((2 + 1) * 3) -> 9
["4", "13", "5", "/", "+"] -> (4 + (13 / 5)) -> 6
"""

def evalRPN(A):
    res = A[0]
    stack = []
    for elem in A:
        if elem not in '+-*/':
            stack.append(elem)
        else:
            b = stack.pop()
            a = stack.pop()
            res = eval(str(a) + elem + str(b))
            stack.append(res)
    return res

if __name__ == '__main__':
    print(evalRPN(["2", "1", "+", "3", "*"]))
    print(evalRPN(["4", "13", "5", "/", "+"]))
