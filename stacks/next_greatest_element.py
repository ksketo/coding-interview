"""
Given an array, find the next greater element G[i] for every element A[i] in the array.
The Next greater Element for an element A[i] is the first greater element on the
right side of A[i] in array.
Elements for which no greater element exist, consider next greater element as -1.

Example:
Input : A : [4, 5, 2, 10]
Output : [5, 10, 10, -1]

Example 2:
Input : A : [3, 2, 1]
Output : [-1, -1, -1]
"""

def nextGreater(A):

if __name__ == '__main__':
    print(nextGreater([4, 5, 2, 10]))
    print(nextGreater([3, 2, 1]))
