def prevSmaller(self, arr):
    ## =============
    ## Time complexity: O(n^2)
    ## =============
    # G = arr[:]
    # for i, elem in enumerate(arr):
    #     j = i
    #     G[i] = -1
    #     while j >= 0:
    #         if arr[j] < arr[i]:
    #             G[i] = arr[j]
    #             break
    #         j -= 1
    # return G

    ## =============
    ## Time complexity: O(n)
    ## =============
    G = []
    stack = []
    for i, elem in enumerate(arr):
        while len(stack)>0 and stack[-1] >= elem:
            stack.pop()
        if len(stack):
            G.append(stack[-1])
        else:
            G.append(-1)
        stack.append(elem)
    return G
