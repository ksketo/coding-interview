def braces(A):
    A = str(A)
    operators = '+-*/'
    A = filter(lambda x: x in operators+'()', A)
    print(A)
    op_stack = []
    for char in A:
        print('char is {}'.format(char))
        if char != ')':
            op_stack.append(char)
        else:
            if op_stack[-1] == '(':
                print(op_stack)
                return 1
            else:
                while op_stack[-1] != '(' and len(op_stack):
                    op_stack.pop()
                if op_stack[-1] == '(':
                    op_stack.pop()
        print('op_stack is: {}'.format(op_stack))
    if len(op_stack):
        return 1
    return 0

if __name__ == '__main__':
    print(braces("(a+(a+b))"))
