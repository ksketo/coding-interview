from collections import OrderedDict

class LRU(object):
    def __init__(self, capacity):
        self.cache = OrderedDict()
        self.capacity = capacity

    def get(self, key):
        if self.cache.has_key(key):
            x = self.cache.pop(key)
            self.cache[key] = x
            return x
        return -1

    def set(self, key, value):
        if self.cache.has_key(key):
            self.cache.pop(key)
            self.cache[key] = value
        else:
            if len(self.cache.values()) >= self.capacity:
                self.cache.popitem(last=False)
                self.cache[key] = value
            else:
                self.cache[key] = value

if __name__ == '__main__':
    lru = LRU(capacity=1)
    print('S 2 1')
    lru.set(2, 1)
    print('G 2')
    print(lru.get(2))
    print('S 3 2')
    lru.set(3, 2)
    print('G 2')
    print(lru.get(2))
    print('G 3')
    print(lru.get(3))
