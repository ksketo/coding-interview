def fib_rec(n):
    return fib_iter(n-1) + fib_iter(n-2) if n > 1 else n

def fib_iter(n):
    old = 0
    new = 1
    for i in range(n):
        # tmp = new
        # new = tmp + old
        # old = tmp
        old, new = new, old + new
    return old

if __name__ == '__main__':
    fib = fib_iter
    fib_seq = [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
    assert fib(1) == fib_seq[1]
    assert fib(2) == fib_seq[2]
    assert fib(3) == fib_seq[3]
    assert fib(5) == fib_seq[5]
    print('all tests pass')
