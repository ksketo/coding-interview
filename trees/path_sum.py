def hasPathWithGivenSum(t, s):
    if not t:
        return s == 0
    if not t.left and not t.right:
        return t.value == s
    return (t.left is not None and hasPathWithGivenSum(t.left, s-t.value)) or (t.right is not None and hasPathWithGivenSum(t.right, s-t.value))
