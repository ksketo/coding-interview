class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

def invertTree(root):
    def invert(root):
        if root is not None:
            root.left, root.right = root.right, root.left
            invert(root.left)
            invert(root.right)
        return
    invert(root)
    return root
