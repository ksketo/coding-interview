# Definition for binary tree:
# class Tree(object):
#   def __init__(self, x):
#     self.value = x
#     self.left = None
#     self.right = None
def isTreeSymmetric(t):
    return isMirror(t, t)

def isMirror(t1, t2):
    if t1 is None and t2 is None:
        return True
    if t1 is not None and t2 is not None:
        if t1.value == t2.value:
            return isMirror(t1.left, t2.right) and isMirror(t1.right, t2.left)
    return False
