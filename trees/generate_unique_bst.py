"""
Given n, generate all structurally unique BST's (binary search trees) that store
values 1...n.
For example given n = 3, your program should return all 5 unique BST's shown below.
   1         3     3      2      1
    \       /     /      / \      \
     3     2     1      1   3      2
    /     /       \                 \
   2     1         2                 3

Reference: http://www.geeksforgeeks.org/construct-all-possible-bsts-for-keys-1-to-n/
"""

def generateTrees(n):
    return generateTrees2(1, n)

def generateTrees2(start, end):
    solution = []
    if start > end:
        solution.append(None)
        return solution
    for root in range(start, end+1):
        leftNodes = generateTrees2(start, root-1)
        rightNodes = generateTrees2(root+1, end)
        for leftNode in leftNodes:
            for rightNode in rightNodes:
                node = TreeNode(root)
                node.left = leftNode
                node.right = rightNode
                solution.append(node)
    return solution
