"""
Convert sorted linked list to BST

References:
- https://discuss.leetcode.com/topic/21414/python-recursive-solution-with-detailed-comments-operate-linked-list-directly
- http://www.geeksforgeeks.org/sorted-linked-list-to-balanced-bst/
"""

def sortedListToBST(head):
    if head is None:
        return
    if head.next is None:
        return TreeNode(head.val)
    # Find middle of list
    slow, fast = head, head.next.next
    while fast and fast.next:
        slow = slow.next
        fast = fast.next.next
    # Create root
    tmp = slow.next
    # Cut down left half
    slow.next = None
    # Recursively constract tree from left and right half
    root = TreeNode(tmp.val)
    root.left = sortedListToBST(head)
    root.right = sortedListToBST(tmp.next)
    return root
