class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

def postOrderTraversalTwoStack(A):
    s1 = []
    s2 = []
    res = []
    s1.append(A)
    while len(s1) > 0:
        current = s1.pop()
        s2.append(current)
        if current.left is not None:
            s1.append(current.left)
        if current.right is not None:
            s1.append(current.right)
    while len(s2) > 0:
        node = s2.pop()
        res.append(node.val)
    return res

def postOrderTraversalRec(A):
    if A is None:
        return []
    return postOrderTraversalRec(A.left) + postOrderTraversalRec(A.right) + [A.val]


if __name__ == '__main__':
    root = TreeNode(1)
    root.left = TreeNode(2)
    root.right = TreeNode(3)
    root.left.left = TreeNode(4)
    root.left.right = TreeNode(5)
    assert postOrderTraversalTwoStack(root) == [4, 5, 2, 3, 1]
    root = TreeNode(1)
    root.right = TreeNode(2)
    root.right.left = TreeNode(3)
    assert postOrderTraversalTwoStack(root) == [3, 2, 1]

#         1
#       /   \
#     2       3
#   /   \
# 4       5
