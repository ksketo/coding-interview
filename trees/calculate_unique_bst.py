"""
Calculate the number of unique BST that store values 1...n.
For example given n = 3, your program should return 5.

Reference:
- https://discuss.leetcode.com/topic/8398/dp-solution-in-6-lines-with-explanation-f-i-n-g-i-1-g-n-i
- https://discuss.leetcode.com/topic/19670/python-solutions-dp-catalan-number
- http://www.geeksforgeeks.org/program-nth-catalan-number/

G[2] = G[0] * G[2-1] + G[1] * G[2-2]
G[n] = G[0] * G[n-1] + G[1] * G[n-2] + ... + G[n-1] * G[0]
"""

def calculateTrees(n):
    G = [0 for i in range(n+1)]
    G[0] = G[1] = 1
    for i in range(2, n+1):
        for j in range(0, i):
            G[i] += G[j]*G[i-1-j]
    return G[n]


if __name__ == '__main__':
    assert calculateTrees(2) == 2
    assert calculateTrees(3) == 5
