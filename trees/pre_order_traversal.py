class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

def preOrderTraversalRec(A):
    if A is None:
        return []
    return [A.val] + preOrderTraversal(A.left) + preOrderTraversal(A.right)

def preOrderTraversal(A):
    res = []
    s = []
    current = A
    s.append(current)
    while len(s) > 0:
        current = s.pop()
        res.append(current.val)
        if current.right is not None:
            s.append(current.right)
        if current.left is not None:
            s.append(current.left)
    return res


if __name__ == '__main__':
    root = TreeNode(1)
    root.left = TreeNode(2)
    root.right = TreeNode(3)
    root.left.left = TreeNode(4)
    root.left.right = TreeNode(5)
    assert preOrderTraversal(root) == [1, 2, 4, 5, 3]
    root = TreeNode(1)
    root.right = TreeNode(2)
    root.right.left = TreeNode(3)
    assert preOrderTraversal(root) == [1, 2, 3]

#         1
#       /   \
#     2       3
#   /   \
# 4       5
