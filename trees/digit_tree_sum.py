"""
We're going to store numbers in a tree. Each node in this tree will store a
single digit (from 0 to 9), and each path from root to leaf encodes a non-negative
integer.
Given a binary tree t, find the sum of all the numbers encoded in it.

References:
- http://www.geeksforgeeks.org/sum-numbers-formed-root-leaf-paths/
"""

def digitTreeSumBFS(t):
    if not t:
        return 0
    s = 0
    nodes = [(t, "")]
    while nodes:
        new_nodes = []
        for node, num in nodes:
            if node.left is None and node.right is None:
                s += int(num + str(node.value))
            if node.left:
                new_nodes.append((node.left, num+str(node.value)))
            if node.right:
                new_nodes.append((node.right, num+str(node.value)))
        nodes = new_nodes
    return s

def digitTreeSumRec(t, val=0):
    if t is None:
        return 0
    val = 10*val + t.value
    if t.right is None and t.left is None:
        return val
    return digitTreeSum(t.left, val) + digitTreeSum(t.right, val)
