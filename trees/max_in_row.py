"""
You have a binary tree t. Your task is to find the largest value in each row of
this tree. In a tree, a row is a set of nodes that have equal depth. For example,
 a row with depth 0 is a tree root, a row with depth 1 is composed of the root's
 children, etc.

Return an array in which the first element is the largest value in the row with
depth 0, the second element is the largest value in the row with depth 1, the
third element is the largest element in the row with depth 2, etc.
"""

def largestValuesInTreeRows(t):
    if t is None:
        return []
    from Queue import Queue
    import sys
    q = Queue()
    res = []
    q.put(t)
    while not q.empty():
        helper = []
        m = -sys.maxsize
        while not q.empty():
            node = q.get()
            helper.append(node)
        for node in helper:
            if node.value > m:
                m = node.value
            if node.left is not None:
                q.put(node.left)
            if node.right is not None:
                q.put(node.right)
        res.append(m)
    return res
