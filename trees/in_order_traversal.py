class Node:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

def inorderTraversal(A):
    s = []
    res = []
    current = A
    while not (len(s) == 0 and current is None):
        if current is not None:
            s.append(current)
            current = current.left
        else:
            if len(s):
                current = s.pop()
                res.append(current.val)
                current = current.right
    return res


if __name__ == '__main__':
    root = Node(1)
    root.left = Node(2)
    root.right = Node(3)
    root.left.left = Node(4)
    root.left.right = Node(5)

    assert inorderTraversal(root) == [4, 2, 5, 1, 3]

#         1
#       /   \
#     2       3
#   /   \
# 4       5
