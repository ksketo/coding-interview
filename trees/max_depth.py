"""
Given a binary tree, find its maximum depth.

The maximum depth of a binary tree is the number of nodes along the longest path
from the root node down to the farthest leaf node.
"""

class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

def max_depth_rec(A):
    if A == None:
        return 0
    return 1 + max(max_depth_rec(A.left), max_depth_rec(A.right))

def max_depth(t):
    if t is None:
        return 0
    s = []
    h = 0
    s.append(t)
    while len(s) > 0:
        helper = s[:]
        if len(s) > 0:
            h += 1
        s = []
        for node in helper:
            if node.left is not None:
                s.append(node.left)
            if node.right is not None:
                s.append(node.right)
    return h


if __name__ == '__main__':
    root = TreeNode(1)
    root.left = TreeNode(2)
    root.right = TreeNode(3)
    root.left.left = TreeNode(4)
    root.left.right = TreeNode(5)
    assert max_depth_rec(root) == 3
    assert max_depth(root) == 3
    root.left.right.right = TreeNode(6)
    assert max_depth(root) == 4
