"""
There is a row of seats. Assume that it contains N seats adjacent to each other.
There is a group of people who are already seated in that row randomly. i.e. some
 are sitting together & some are scattered.

An occupied seat is marked with a character 'x' and an unoccupied seat is marked
with a dot ('.')

Now your target is to make the whole group sit together i.e. next to each other,
without having any vacant seat between them in such a way that the total number
of hops or jumps to move them should be minimum.

Return minimum value % MOD where MOD = 10000003
"""

def seats(A):
    s = []
    MOD = 10000003

    # find array of seats
    for i, seat in enumerate(A):
        if seat == 'x':
            s.append(i)

    print(s)
    # find median
    mid = len(s) / 2;
    if len(s) % 2 == 1:
        median = s[mid]
    else:
        median = (s[mid] + s[mid - 1])/2

    print(median)
    ans = 0

    # calculate movements in first half
    if A[median] == 'x':
        empty = median - 1
    else:
        empty = median

    for i in range(0, median - 1):
        if (A[i] == 'x'):
            ans += empty - i

            if(ans >= MOD):
                ans %= MOD
            empty -= 1

    print('ans: {}'.format(ans))
    # calculate movements in second half
    empty = median + 1
    for i in range(median + 1, len(A)):
        if A[i] == 'x':
            ans += i - empty
            if ans >= MOD:
                ans %= MOD
            empty += 1

    return ans

if __name__ == '__main__':
    print seats("....x..xx...x..")
    print seats(".x.x")
