# Maximum sum such as not two elements are adjacent

sum[i] = max(sum[i - 1], sum[i - 2] + nums[i])

a = 0
b = 0
for num in nums:
  a, b = num + b, max(a, b)
