"""
Description
---------------
You are planning to rob houses on a specific street, and you know that every house
on the street has a certain amount of money hidden. The only thing stopping you
from robbing all of them in one night is that adjacent houses on the street have
a connected security system. The system will automatically trigger an alarm if
two adjacent houses are broken into on the same night.

Given a list of non-negative integers nums representing the amount of money hidden
in each house, determine the maximum amount of money you can rob in one night
without triggering an alarm.

Example
---------------
For nums = [1, 1, 1], the output should be houseRobber(nums) = 2.

The optimal way to get the most money in one night is to rob the first and the
third houses for a total of 2.
"""

def houseRobberRec(nums):
    if len(nums) == 0:
        return 0
    if len(nums) == 1:
        return nums[0]
    return max(nums[0] + houseRobber(nums[2:]), houseRobber(nums[1]))

def houseRobberMinimal(nums):
    a = b = 0
    for num in nums:
        new_b = max(a, b)
        a = b + num
        b = new_b
    return max(a, b)

def houseRobber(nums):
    if len(nums) == 0:
        return 0
    if len(nums) == 1:
        return nums[0]

    old, new = nums[0], max(nums[0], nums[1])

    for num in nums[2:]:
        tmp = new
        new = max(old+num, new)
        old = tmp
    return max(new, old)
