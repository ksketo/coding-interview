import unittest

def calculate_loss (pricesLst):
    """Calculate the largest possible loss a client could have made with only a
    buy transaction followed by a sell transaction.

    Arguments:
        pricesLst(list): very large list of prices (values should be positive)

    Returns:
        loss(int): the largest possible loss a client could have made. If there
        is not loss, the output is 0. If the input is empty or negative value is
        found in list, the output is -1
    """
    if len(pricesLst) <= 1:
        return -1
    max_price = pricesLst[0]
    min_price = pricesLst[0]
    loss = 0
    if max_price < 0:
        return -1
    for price in pricesLst[1:]:
        if price < 0:
            return -1
        if price > max_price:
            max_price = price
            min_price = price
        if price < min_price:
            min_price = price
        if max_price - min_price > loss:
            loss = max_price - min_price
    if loss > 0:
        return loss
    else:
        return 0

class TestCalculateLoss(unittest.TestCase):
    def test_calculate_loss (self):
        self.assertEqual(calculate_loss([]), -1)
        self.assertEqual(calculate_loss([1]), -1)
        self.assertEqual(calculate_loss([5, 5]), 0)
        self.assertEqual(calculate_loss([5, 10, 10, 15, 20]), 0)
        self.assertEqual(calculate_loss([0, 10, 5, 11, 5]), 6)
        self.assertEqual(calculate_loss([0, 10, 5, 1, 11, 5]), 9)
        self.assertEqual(calculate_loss([0, 10, 5, 1, 11, 5, 1]), 10)
        self.assertEqual(calculate_loss([-1, 10, 5]), -1)


if __name__ == '__main__':
    unittest.main()
