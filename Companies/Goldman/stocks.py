import unittest

def calculate_loss (pricesLst):
    if len(pricesLst) <= 1:
        return -1
    buy = pricesLst[0]
    sell = pricesLst[0]
    diff = 0
    for i in range(1,len(pricesLst)):
        if pricesLst[i] > buy:
            buy = pricesLst[i]
            sell = pricesLst[i]
        if pricesLst[i] < sell:
            sell = pricesLst[i]
        if buy - sell > diff:
            diff = buy - sell
    return diff


class TestCalculateLoss(unittest.TestCase):
    def test_calculate_loss (self):
        self.assertEqual(calculate_loss([]), -1)
        self.assertEqual(calculate_loss([1]), -1)
        self.assertEqual(calculate_loss([5, 5]), 0)
        self.assertEqual(calculate_loss([5, 10, 10, 15, 20]), 0)
        self.assertEqual(calculate_loss([0, 10, 5, 11, 5]), 6)
        self.assertEqual(calculate_loss([0, 10, 5, 1, 11, 5]), 9)
        self.assertEqual(calculate_loss([0, 10, 5, 1, 11, 5, 1]), 10)
        # self.assertEqual(calculate_loss([-1, 10, 5]), -1)


if __name__ == '__main__':
    unittest.main()
