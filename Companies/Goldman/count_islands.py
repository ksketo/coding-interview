def count_islands(graph):
    count = 0
    row = len(graph)
    col = len(graph[0])
    visited = [[0 for j in range(col)] for i in range(row)]
    for i in range(row):
        for j in range(col):
            if visited[i][j] == 0 and graph[i][j] == 1:
                DFS(i, j, graph, visited)
                count += 1
    return count

def DFS(i, j, graph, visited):
    visited[i][j] = 1
    # find combinations of movements
    a = [-1, 0, 1, -1, 1, -1, 0, 1]
    b = [-1, -1, -1, 0, 0, 1, 1, 1]

    # for each movement
    for k in range(8):
        if is_safe(i+a[k], j+b[k], graph, visited):
            DFS(i+a[k], j+b[k], graph, visited)
    return

def is_safe(i, j, graph, visited):
    if i < 0 or i > len(graph)-1 or j < 0 or j > len(graph) - 1:
        return False
    if graph[i][j] == 0:
        return False
    if visited[i][j] == 1:
        return False
    return True

if __name__ == '__main__':
    graph = [[1, 1, 0, 0, 0],
            [0, 1, 0, 0, 1],
            [1, 0, 0, 1, 1],
            [0, 0, 0, 0, 0],
            [1, 0, 1, 0, 1]]
    # g= Graph(row, col, graph)
    assert count_islands(graph) == 5
