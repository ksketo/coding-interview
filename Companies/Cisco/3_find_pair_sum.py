#!/usr/bin/python
"""
==========
Notes
==========
- if pair is not found return -1
- if any of the arrays is empty return -1
- if more then one pairs are found, return the pair with the smallest a index
- sum and numbers can be any integer (positive or negative)
- we assume that inputs will have the correct type (a, b type of list and k integer),
  all elements of array will be integers and arrays will be sorted

==========
Complexity
==========
Time complexity: O(n)
Space complexity: O(1)
"""

# use two pointers to take advantage of the sorting
def find_pair(a, b, k):
    """Find the pair of indices (one into each array) which identify elements
    which sum to a given integer

    Arguments:
        a (list): first input sorted array of integers
        b (list): second input sorted array of integers
        k (integer): sum to search for in arrays

    Returns:
        tuple with pair of indices
    """
    if not len(a) or not len(b):
        return -1
    l = 0
    r = len(b) - 1
    while l < len(a) and r >= 0:
        if a[l] + b[r] > k:
            r -= 1
        elif a[l] + b[r] < k:
            l += 1
        else:
            return (l, r)
    return -1


if __name__ == '__main__':
    assert find_pair([], [1], 1) == -1
    assert find_pair([1], [], 1) == -1
    assert find_pair([], [], 0) == -1
    assert find_pair([1], [1], 2) == (0, 0)
    assert find_pair([-1, 1, 2], [0, 1, 3], 3) == (2, 1)
    assert find_pair([1, 4, 5, 7], [10, 20, 30, 40], 37) == (3, 2)
    assert find_pair([1, 4, 5, 7, 30], [7, 10, 20, 30, 40], 37) == (3, 3)
    assert find_pair([1, 4, 5, 7, 30], [7, 10, 20, 30, 40], 37) != (4, 0)
    assert find_pair([1, 4, 5, 7], [10, 20, 30, 40], 36) == -1
    assert find_pair([-1, 1, 3], [-4, -3, 5, 10], -4) == (0, 1)
    print('**Tests passed successfully**')
