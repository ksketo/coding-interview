#!/usr/bin/python
"""
==========
Notes
==========
- if array is empty returns -1
- since we are not speaking for strictly monotonically increasing, two adjacent
  elements with same value, are considered part of the subsequence
- if more then one subseq found, then keep the one with the smallest starting index

==========
Complexity
==========
Time complexity: O(n)
Space complexity: O(1)
"""

def find_subseq(s):
    """Finds the longest monotonically increasing contiguous subsequence and reverses it

    Arguments:
        s (list): sequence of integers

    Returns:
        list of integers
    """
    if not len(s):
        return -1
    start, max_start = 0, 0
    end, max_end = 0, 0
    for i in range(1, len(s)):
        if s[i] >= s[i-1]:
            end = i
        else:
            start = i
            end = i
        if (end - start) > (max_end - max_start):
            max_end, max_start = end, start
    return s[max_start:max_end+1][::-1]


if __name__ == '__main__':
    assert find_subseq([]) == -1
    assert find_subseq([3, 2, 1]) == [3]
    assert find_subseq([4, 5, 6, 1, 2, 13]) == [6, 5, 4]
    assert find_subseq([10, 1, 3, 2, 2, 7, 10, 200, 1]) == [200, 10, 7, 2, 2]
    assert find_subseq([10, 1, 3, 2, 2, 7, 10, 200]) == [200, 10, 7, 2, 2]
    print('**Tests passed successfully**')
