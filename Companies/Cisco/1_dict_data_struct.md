## Linked lists

Implementation of dictionaries with linked lists can be done with skip lists.
Insertion, removal and retrieval is done in	O(log N) time.

+ An advantage skip lists have is that they can be used as the underlying storage
for a sorted set data structure, which is important for real word dictionaries.
- Their disadvantage is that O(log N) is slower then what we can achieve with hash
tables.

## Arrays

Implementations of dictionaries with arrays can be done with hash tables
Insertion, removal and retrieval is done in	O(1) time (unless we have to search
in a bucket of keys for a specific entry, due to collision, which would take O(n)).

+ Hash tables advantage is that they can be very fast (O(1)).
- However, in case of many collisions they have worse time complexity (O(n)) then the
linked list implementation. Also they cannot preserve order.

## Other data structures

**AVL trees*** and **Red-Black trees**. Insertion, retrieval and removal takes
O(log N) time. Their disadvantage is that both these structures must be balanced
after insertions and removals.

## Tries and ternary search trees

For the dictionary implementation we could use **Tries**.

+ Insertion, removal and retrieval is done in	O(h) time, where h is the length
of  the word to be processed.  Another advantage of Trie is, we can print all words
in alphabetical order which is not possible with hashing. With Tries we can also
do prefix search which is not possible with the other implementations.
- The disadvantage of Trie is, it requires lots of space.

Alternatively, we could use **Ternary Search Trees**. They support operations
supported by Trie like prefix search, alphabetical order printing and nearest
neighbor search and time complexity of search operation is O(h) where h is height
of the tree.
