https://www.hackerrank.com/contests/juniper-hackathon/challenges/metals/forum

def  maxProfit(costPerCut, salePrice, lengths):
    max_length = max(lengths)
    max_profit = 0

    for length in range(1, max_length):
        profit = 0
        for i in range(len(lengths)):
            if length > lengths[i]:
                continue
            currPrice = (lengths[i] / length) * salePrice * length
            if lengths[i] % length == 0:
                cuts = lengths[i] / length -1
            else:
                cuts = lengths[i] / length

            currProfit = currPrice - costPerCut * cuts;
            if (currProfit > 0):
                profit += currProfit

        if profit > max_profit:
            max_profit = profit
    return max_profit

def find_anagram(s):
    if len(s)%2 == 1:
        return -1
    mid = len(s) / 2
    s1 = s[:mid]
    s2 = s[mid:]

    s1_dict = {}
    for char in s1:
        if s1_dict.has_key(char):
            s1_dict[char] += 1
        else:
            s1_dict[char] = 1
    s2_dict = {}
    for char in s2:
        if s2_dict.has_key(char):
            s2_dict[char] += 1
        else:
            s2_dict[char] = 1

    counter = 0
    for key in s2_dict.keys():
        if not s1_dict.has_key(key):
            counter += s2_dict[key]
        else:
            counter += max(0, s2_dict[key] - s1_dict[key])

    return counter

if __name__ == '__main__':
    t = input()
    for _ in xrange(t):
        s = raw_input()
        print(find_anagram(s))
