# -------------
# Description
# -------------
# You are given a simple graph and the number of vertices in it, n.
# Return the number of complete subgraphs of the given graph that have more
# than two vertices. A subgraph is said to be complete if all the vertices
# are connected by an edge.
#
# -------------
# Resources
# -------------
# - Complete Graphs: http://mathworld.wolfram.com/CompleteGraph.html
# - Graph Theory: http://mathworld.wolfram.com/topics/GraphTheory.html
#
# -------------
# Example
# -------------
# For n = 6 and
# graph = [['A', 'B'], ['B', 'C'], ['B', 'D'], ['C', 'D'], ['E', 'F'], ['F', 'B'], ['F', 'C'], ['F', 'D']],
# the output should be completeSubgraphs(n, graph) = 5.
# There are five complete subgraphs: BDC, BDF, BCF, DCF, and BDCF.
# -------------
# Solution
# -------------
# https://codefights.com/challenge/tK2Zzw5WjkSpbFuRv/solutions/gkadiWRg99LwM2nER

def completeSubgraphs(n, g):
    r = [""]
    for c in 'ABCDEFGHIJKLMNO':
        print([s+c for s in r if all([i, c] in g or [c, i] in g for i in s)])
        r += [s+c for s in r if all([i, c] in g or [c, i] in g for i in s)]

    c = 0
    for s in r:
        c += len(s)>2
    return c
