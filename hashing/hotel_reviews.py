from collections import Counter

def hotel_reviews(A, B):
    a_set = set(A.split('_'))
    word_dict = {}
    for i, words in enumerate(B):
        words_set = set(words.split('_'))
        word_dict[i] = len(a_set & words_set)
    return map(lambda x: x[0], sorted(word_dict.items(), key=lambda x: x[1], reverse=True))

if __name__ == '__main__':
    A = "cool_ice_wifi"
    B = ["water_is_cool", "cold_ice_drink", "cool_wifi_speed", "cool_ice_drink"]
    assert hotel_reviews(A, B) == [2, 3, 0, 1]
