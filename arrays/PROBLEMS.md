# Duplicates & Repeating Elements

- **Find first duplicate**
- **Find first non repeating element**

# Array Rotation

- **In place rotation in C**

http://www.geeksforgeeks.org/inplace-rotate-square-matrix-by-90-degrees/

- **Rotate array clockwise**
```bash
# one liner
zip(*a[::-1])
```
- **Rotate array counterclockwise**
```bash
# one liner
zip(*origin)[::-1]
```

# Sudoku

- **Sudoku validation**
- **Sudoku Solver (Backtracking)**
http://www.geeksforgeeks.org/backtracking-set-7-suduku/
