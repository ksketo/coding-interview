"""
Given a set s and a number k find all subsets of size k.
"""

from itertools import combinations

def subsets_rec(s, k):
    if k == 0:
        return [[]]
    if len(s) <= k:
        return [s]
    res = []
    tmp = s[:]
    for i in range(1, len(s)+1):
        elem = s[i-1]
        tmp = s[i:]
        res += [[elem] + x for x in subsets_rec(tmp, k-1)] + subsets_rec(tmp, k)
    return res
    # return [[elem] + subset for subset in subsets_rec(s[i:], k-1) for i, elem in enumerate(s)]
    # return [pre + [val] for i, val in enumerate(s) for pre in subsets_rec(s[i:], k-1)]

def combine(n, k):
    if k == 0:
        return [[]]
    return [pre + [i] for i in range(1, n+1) for pre in combine(i-1, k-1)]

if __name__ == '__main__':
    arr = [1, 2, 3, 4, 5]
    k = 3
    print(subsets_rec(arr, k))
    # assert subsets_rec(arr, k) == list(combinations(arr, k))
    # print(combine(5, 3))
    # assert combine(5, 3) == list(combinations(arr, k))
