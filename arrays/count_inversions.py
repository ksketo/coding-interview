class Solution:
    # @param A : list of integers
    # @return an integer
    def countInversions(self, A):
        self.count = 0
        def merge(left, right):
            i = j = 0

            if len(left) < 1 or len(right) < 1:
                return left or right

            res = []
            while len(res) < len(left) + len(right):
                if left[i] <= right[j]:
                    res.append(left[i])
                    i += 1
                else:
                    self.count += len(left) - i
                    res.append(right[j])
                    j += 1
                if i == len(left) or j == len(right):
                    res.extend(left[i:] or right[j:])
                    break

            return res

        def mergesort(a):
            if len(a) < 2:
                return a

            middle = len(a)/2

            left = mergesort(a[:middle])
            right = mergesort(a[middle:])

            return merge(left, right)

        mergesort(A)
        return self.count

if __name__ == '__main__':
    sol = Solution()
    arr = [1, 20, 6, 4, 5]
    assert sol.countInversions(arr) == 5
    assert sol.countInversions([1, 20, 6, 4, 5, 2]) == 9
    assert sol.countInversions([1, 20, 6, 4, 5, 2, 1]) == 14
