"""
Return index of element if exists in array or return index of position where
it should be inserted in sorted array
"""

# def insert_sorted(nums, target):
#     start = 0
#     end = len(nums) - 1
#     while start <= end:
#         mid = (start + end) / 2
#         if nums[mid] == target:
#             return mid
#         if nums[mid] > target:
#             end = mid - 1
#         else:
#             start = mid + 1
#     return start

def insert_sorted(nums, target):
    start = 0
    end = len(nums) - 1
    while start < end:
        mid = (start + end) / 2
        if nums[mid] > target:
            end = mid
        else:
            start = mid + 1
    return start

if __name__ == '__main__':
    assert insert_sorted([1, 3], 0) == 0
    assert insert_sorted([1, 2, 7, 8], 4) == 2
    print('tests pass')
