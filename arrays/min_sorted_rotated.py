def findMin(nums):
    """
    :type nums: List[int]
    :rtype: int
    """
    l = 0
    h = len(nums) - 1
    while nums[l] > nums[h]:
        mid = (l + h) / 2
        if nums[mid] > nums[h]:
            l = mid + 1
        else:
            h = mid
    return nums[l]
