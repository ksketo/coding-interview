"""
Given a string s, find and return the first instance of a non-repeating
character in it. If there is no such character, return '_'.

Note: Write a solution that only iterates over the string once and uses O(1)
additional memory, since this is what you would be asked to do during a real
interview.
"""

def non_rep_nstor(s):
    d = {}
    for i, elem in enumerate(s):
        if d.has_key(elem):
            count = d[elem][0]
            d[elem] = (count, i)
        else:
            d[elem] = (1, i)

    srt = sorted(d.items(), key=lambda x: x[1][1])
    return srt[0][0]

def non_rep(s):
    pass


if __name__ == '__main__':
    s = "abacabad"
    assert non_rep(s) == 'c'
    print('Tests run successfully')
