"""
References:
- https://en.wikipedia.org/wiki/Heapsort
- http://www.geeksforgeeks.org/heap-sort/
"""

def heapify(a, first, last):
    "Puts elements of 'a' in heap order in place"
    largest = first * 2 + 1
    while largest <= last:
        # Check that right element exists
        if largest < last and a[largest] < a[largest+1]:
            largest += 1

        # Swap elements
        if a[first] < a[largest]:
            a[first], a[largest] = a[largest], a[first]
            first = largest
            largest = 2 * first + 1
        else:
            return

def heapsort(a):
    # build max heap so that contains max element at root
    for i in range(len(a)/2, -1, -1):
        heapify(a, i, len(a)-1)

    for i in range(len(a)-1, 0, -1):
        if a[0] > a[i]:
            # place max element at the end of the array
            a[i], a[0] = a[0], a[i]
            # update heap to find next max element in the remaining array
            heapify(a, 0, i-1)
    return a

# # Python program for implementation of heap Sort
#
# # To heapify subtree rooted at index i.
# # n is size of heap
# def heapify(arr, n, i):
#     largest = i  # Initialize largest as root
#     l = 2 * i + 1     # left = 2*i + 1
#     r = 2 * i + 2     # right = 2*i + 2
#
#     # See if left child of root exists and is
#     # greater than root
#     if l < n and arr[i] < arr[l]:
#         largest = l
#
#     # See if right child of root exists and is
#     # greater than root
#     if r < n and arr[largest] < arr[r]:
#         largest = r
#
#     # Change root, if needed
#     if largest != i:
#         arr[i],arr[largest] = arr[largest],arr[i]  # swap
#
#         # Heapify the root.
#         heapify(arr, n, largest)
#
# # The main function to sort an array of given size
# def heapSort(arr):
#     n = len(arr)
#
#     # Build a maxheap.
#     for i in range(n, -1, -1):
#         heapify(arr, n, i)
#
#     # One by one extract elements
#     for i in range(n-1, 0, -1):
#         arr[i], arr[0] = arr[0], arr[i]   # swap
#         heapify(arr, i, 0)
#
# # Driver code to test above
# arr = [ 12, 11, 13, 5, 6, 7]
# heapSort(arr)
# n = len(arr)
# print ("Sorted array is")
# for i in range(n):
#     print ("%d" %arr[i]),

if __name__ == '__main__':
    a = [3,4,5,1,2,8,3,7,6]
    assert heapsort([]) == []
    assert heapsort([2, 1]) == [1, 2]
    assert heapsort(a) == sorted(a)
    print('Heapsort works as expected')
