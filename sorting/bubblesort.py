def bubblesort(arr):
    need_swap = True
    while need_swap:
        need_swap = False
        for i in range(len(arr)-1):
            if arr[i] > arr[i+1]:
                arr[i], arr[i+1] = arr[i+1], arr[i]
                need_swap = True
    return arr

if __name__ == "__main__":
	print bubblesort([3,4,5,1,2,8,3,7,6])
