def merge(left, right):
    i = j = 0

    if len(left) < 1 or len(right) < 1:
        return left or right

    res = []
    while len(res) < len(left) + len(right):
        if left[i] < right[j]:
            res.append(left[i])
            i += 1
        else:
            res.append(right[j])
            j += 1
        if i == len(left) or j == len(right):
            res.extend(left[i:] or right[j:])
            break

    return res

def mergesort(a):
    if len(a) < 2:
        return a

    middle = len(a)/2

    left = mergesort(a[:middle])
    right = mergesort(a[middle:])

    return merge(left, right)


if __name__ == "__main__":
	print mergesort([3,4,5,1,2,8,3,7,6])
