"""
===========
Description
===========
For array with duplicates return all unique permutations

===========
Example
===========
For [1,1,2] have the following unique permutations:
[1,1,2]
[1,2,1]
[2,1,1]
"""

import itertools

def unique_perm(arr):
    perms = [[]]
    for num in arr:
        new_perms = []
        for perm in perms:
            for i in xrange(len(perm)+1):
                new_perms.append(perm[:i] + [num] + perm[i:])
        perms = new_perms
    return list(set(tuple(p) for p in perms))
    # return list(set(itertools.permutations(arr)))

if __name__ == '__main__':
    print(unique_perm([1, 1, 2]))
