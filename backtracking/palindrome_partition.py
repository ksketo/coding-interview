"""
===========
Description
===========
Given a string s, partition s such that every string of the partition is a palindrome.
Return all possible palindrome partitioning of s.

===========
Example
===========
Given s = "aab",
Return
[
["a","a","b"]
["aa","b"],
]
"""
def ppartition(s):
    return [[s[:i]] + rest
            for i in xrange(1, len(s)+1)
            if s[:i] == s[i-1::-1]
            for rest in ppartition(s[i:])] or [[]]

# def ppartition_b(s):
#     perms = [[]]
#     for i in xrange(1, len(s)+1):
#         new_perms = []

if __name__ == '__main__':
    print(ppartition("aab"))
    print(ppartition("ppartition"))
