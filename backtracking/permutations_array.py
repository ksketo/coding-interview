"""
Description:
Find all permutations of an array

Other solutions:
- https://discuss.leetcode.com/topic/17277/one-liners-in-python
- https://discuss.leetcode.com/topic/21151/simple-python-solution-dfs
-
"""

def permute(nums):
    perms = [[]]
    for n in nums:
        new_perms = []
        for perm in perms:
            for i in xrange(len(perm)+1):
                new_perms.append(perm[:i] + [n] + perm[i:])   ###insert n
        perms = new_perms
    return perms

if __name__ == '__main__':
    print(permute([1]))
    print(permute([1, 2, 3]))
